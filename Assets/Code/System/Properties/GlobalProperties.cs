namespace Code.System.Properties
{
    public static class GlobalProperties
    {
        private const int worldTileSize = 2;
        private const int maxResourceHeld = 20;


        public static int WorldTileSize => worldTileSize;

        public static int MAXResourceHeld => maxResourceHeld;
    }
}
