namespace Code.Player.Tools
{
    public abstract class PlayerTool
    {
        public abstract void UseTool();
    }
}
