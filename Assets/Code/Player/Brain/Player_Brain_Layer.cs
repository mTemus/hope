using UnityEngine;

namespace Code.Player.Brain
{
    public abstract class Player_Brain_Layer : MonoBehaviour
    {
        public abstract void Initialize(Player_Brain brain);
    }
}
